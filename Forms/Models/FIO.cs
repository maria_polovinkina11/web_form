﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forms.Models
{
    public class FIO
    {
      public  int Id { get; set; }//id пользователя в БД
      public string surname { get; set; }//фамилия 
      public string name { get; set; }//имя
      public string patronymic { get; set; }//отчество
    }
}
